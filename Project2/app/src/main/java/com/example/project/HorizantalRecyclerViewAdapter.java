package com.example.project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HorizantalRecyclerViewAdapter extends RecyclerView.Adapter<HorizantalRecyclerViewAdapter.viewholder> {

    int[][] arr = new int[8][5];

    public HorizantalRecyclerViewAdapter(int arr[][]) {
        this.arr = arr;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_model, parent, false);
        return new viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        int type = arr[position][0];
        if (type == 0) {

        } else if (type == 1) {
            holder.imageView.setImageResource(R.drawable.bomb_background);
        } else if (type == 2) {
            holder.imageView.setImageResource(R.drawable.start_background);
        } else if (type == 3) {
            holder.imageView.setImageResource(R.drawable.finish_background);
        }
    }

    @Override
    public int getItemCount() {
        return arr.length;
    }

    class viewholder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public viewholder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imview);

        }
    }
}
