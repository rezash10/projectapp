package com.example.project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.logging.Level;

public class MainActivity extends AppCompatActivity {
    public static String type;
    public static final int LENGTH = 64;
    public static  int startFlag = 0;
    public static  int endFlag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = findViewById(R.id.spinner);
        Button btnShow = findViewById(R.id.btnShow);
        Button clearGraph = findViewById(R.id.btn_delete);
        TextView textView = findViewById(R.id.textView);

        ArrayAdapter<CharSequence> spinnerAdp = ArrayAdapter.createFromResource(this, R.array.spinnerItems, android.R.layout.simple_spinner_item);
        spinnerAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdp);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemStr = parent.getItemAtPosition(position).toString();
                type = itemStr;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 8);
        recyclerView.setHasFixedSize(true);
        int[][] arr = new int[64][6];
        makeArr(arr);
        VerticalRecyclerViewAdapter verticalRecyclerViewAdapter = new VerticalRecyclerViewAdapter(arr, new handleClicks() {

            @Override
            public void pos(int index) {
                if (type.equals("موانع")) {
                    arr[index][0] = 1;
                }else{
                    if (type.equals("نقطه اغاز")&&startFlag!=0) {
                        arr[index][0] = 2;
                        Log.i("mainreza", "pos: ");
                        startFlag =0  ;
                    }
                    else if(type.equals("نقطه پایان")&&endFlag!=0) {
                        arr[index][0] = 3;
                        endFlag = 0;
                    }else{
                        Toast.makeText(MainActivity.this,"نقطه شروع و پایان در نقشه موجود هستند",Toast.LENGTH_LONG).show();

                    }
                }


                recyclerView.getAdapter().notifyItemChanged(index);

            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(verticalRecyclerViewAdapter);


        int predec[] = new int[LENGTH];
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(startFlag==0&&endFlag==0){
                    int start = 0, finish = 0;
                    for (int i = 0; i < LENGTH; i++) {
                        if (arr[i][0] == 2) {
                            start = i;
                        }
                        if (arr[i][0] == 3) {
                            finish = i;
                        }
                    }

                    boolean bfs = bfsSearch(start, finish, arr, predec);
                    if (bfs) {
                        int[] pathNumbers = new int[LENGTH];
                        int container = finish;
                        pathNumbers[0] = container;
                        int counter = 1;
                        while (container != start) {
                            pathNumbers[counter] = predec[container];
                            container = predec[container];
                            counter++;
                        }
                        char[] sides = new char[counter];
                        int sidesCounter = 0;
                        pathNumbers = Arrays.copyOf(pathNumbers, counter);
                        for (int i = pathNumbers.length - 1; i > 0; i--) {
                            if (pathNumbers[i - 1] - pathNumbers[i] == 8) {
                                arr[pathNumbers[i]][5] = 2;
                                sides[sidesCounter] = 'D';
                            } else if (pathNumbers[i - 1] - pathNumbers[i] == -8) {
                                arr[pathNumbers[i]][5] = 1;
                                sides[sidesCounter] = 'U';
                            } else if (pathNumbers[i - 1] - pathNumbers[i] == 1) {
                                arr[pathNumbers[i]][5] = 3;
                                sides[sidesCounter] = 'R';
                            } else if (pathNumbers[i - 1] - pathNumbers[i] == -1) {
                                arr[pathNumbers[i]][5] = 4;
                                sides[sidesCounter] = 'L';

                            }
                            sidesCounter++;
                            recyclerView.getAdapter().notifyItemChanged(pathNumbers[i]);
                        }

                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < sides.length; i++) {
                            stringBuilder.append(sides[i]);
                            System.out.println(sides[i]);
                        }

                        textView.setVisibility(View.VISIBLE);
                        textView.setText(stringBuilder.toString());


                    }
                    else{
                        Toast.makeText(MainActivity.this,"هیچ مسیری وجود ندارد",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(MainActivity.this,"یکی از نقاط شروع و پایان وجود ندارد",Toast.LENGTH_LONG).show();
                }

            }
        });
        clearGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFlag = 1;
                endFlag = 1;
                textView.setText("");
                for (int i = 0; i <LENGTH ; i++) {
                    arr[i][0] = 0;
                    arr[i][5] = 0;

                }
                recyclerView.setItemAnimator(null);
                recyclerView.getAdapter().notifyDataSetChanged();


            }
        });


    }

    public static void makeArr(int[][] arr) {
        arr[0][0] = 2;
        arr[1][0] = 1;
        arr[13][0] = 1;
        arr[19][0] = 1;
        arr[32][0] = 1;
        arr[37][0] = 1;
        arr[41][0] = 1;
        arr[45][0] = 1;
        arr[46][0] = 3;
        arr[47][0] = 1;
        arr[50][0] = 1;
        arr[53][0] = 1;


        int leftCounter = 0;
        int rightCounter = 7;
        for (int i = 0; i < 64; i++) {
            if (i < 8) {
                if (i == 0) {
                    arr[i][1] = i + 1;
                    arr[i][2] = -1;
                    arr[i][3] = -1;
                    arr[i][4] = i + 8;
                } else if (i == 7) {
                    arr[i][1] = -1;
                    arr[i][2] = i - 1;
                    arr[i][3] = -1;
                    arr[i][4] = i + 8;
                } else {
                    arr[i][1] = i + 1;
                    arr[i][2] = i - 1;
                    arr[i][3] = -1;
                    arr[i][4] = i + 8;
                }
            }
            else if (i > 55) {
                if (i == 56) {
                    arr[i][1] = i + 1;
                    arr[i][2] = -1;
                    arr[i][3] = i - 8;
                    arr[i][4] = -1;
                } else if (i == 63) {
                    arr[i][1] = -1;
                    arr[i][2] = i - 1;
                    arr[i][3] = i - 8;
                    arr[i][4] = -1;
                } else {
                    arr[i][1] = i + 1;
                    arr[i][2] = i - 1;
                    arr[i][3] = i - 8;
                    arr[i][4] = -1;
                }
            }
            else if (leftCounter + 8 == i) {
                arr[i][1] = i + 1;
                arr[i][2] = -1;
                arr[i][3] = i - 8;
                arr[i][4] = i + 8;
                leftCounter += 8;
            } else if (rightCounter + 8 == i) {
                arr[i][1] = -1;
                arr[i][2] = i - 1;
                arr[i][3] = i - 8;
                arr[i][4] = i + 8;
                rightCounter += 8;
            } else {
                arr[i][1] = i + 1;
                arr[i][2] = i - 1;
                arr[i][3] = i - 8;
                arr[i][4] = i + 8;
            }


        }
    }

    private static boolean bfsSearch(int start, int finish, int array[][]
            , int[] visitIndexArr) {
        boolean[] visited = new boolean[LENGTH];
        for (int i = 0; i < LENGTH; i++) {
            visited[i] = false;

            visitIndexArr[i] = -2;
        }
        int[] stack = new int[LENGTH+1];
        int stackCounter = 0;
        stack[stackCounter] = start;
        stackCounter++;
        int counter = 0;
        while (stackCounter != counter) {
            int node = stack[counter];
            for (int i = 1; i < 5; i++) {
                if (array[node][i] != -1) {
                    Log.i("main4321",String.valueOf(array[node][i]));
                    if (array[array[node][i]][0] != 1) {
                        if (visited[array[node][i]] == false) {
                            visited[array[node][i]] = true;
                            visitIndexArr[array[node][i]] = node;
                            stack[stackCounter] = array[node][i];
                            stackCounter++;
                            if (array[node][i] == finish) {
                                return true;
                            }
                        }
                    }
                }
            }
            counter++;
        }
        return false;

    }


}