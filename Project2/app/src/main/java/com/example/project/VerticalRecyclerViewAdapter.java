package com.example.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class VerticalRecyclerViewAdapter extends RecyclerView.Adapter<VerticalRecyclerViewAdapter.viewholder> {
    int[][] arr;
    handleClicks clicks;

    public VerticalRecyclerViewAdapter(int[][] arr, handleClicks clicks) {
        this.arr = arr;
        this.clicks = clicks;
    }


    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_model, parent, false);
        return new viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        int type = arr[position][0];
        if (type == 0) {
            changeSrc(holder.imageView,arr[position][5]);
        } else if (type == 1) {
            holder.imageView.setImageResource(R.drawable.bomb_background);
        } else if (type == 2) {
            holder.imageView.setImageResource(R.drawable.start_background);
        } else if (type == 3) {

            holder.imageView.setImageResource(R.drawable.finish_background);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             clicks.pos(position);
            }
        });








    }

    @Override
    public int getItemCount() {
        return arr.length;
    }

    class viewholder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public viewholder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imview);
        }
    }

    public void changeSrc(ImageView imageView,int sideNumber){
        switch (sideNumber){
            case 1:{
                imageView.setImageResource(R.drawable.ic_top_arrow);
                break;
            }
            case 2:{
                imageView.setImageResource(R.drawable.ic_down_arrow);
                break;
            }
            case 3:{
                imageView.setImageResource(R.drawable.ic_baseline_arrow_right_24);
                break;

            }
            case 4:{
                imageView.setImageResource(R.drawable.ic_left_arrow);
                break;

            }


        }
    }

}

